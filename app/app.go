package main

import (
	"flag"
	"fmt"
	"io/ioutil"

	"os"

	"bitbucket.org/oves/wemon"
	"github.com/ghodss/yaml"
)

// Binding metar data to MQTT topic
type Binding struct {
	Icao  string `yaml:"icao"`
	Check string `yaml:"check"`
	Topic string `yaml:"topic"`
}

type config struct {
	Metar wemon.ConfigMetar `yaml:"metar"`
	Mqtt  wemon.ConfigMqtt  `yaml:"mqtt"`
	Bind  []Binding         `yaml:"bind"`
}

func readConfig(cfgPath string) (config, error) {
	var cfg config
	file, err := ioutil.ReadFile(cfgPath)
	// if error
	if err != nil {
		return cfg, err
	}

	yaml.Unmarshal(file, &cfg)

	return cfg, err
}

func checkError(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func main() {
	config := flag.String("c", "config.yml", "path to configuration file")
	verbose := flag.Bool("v", false, "enable verbose")

	flag.Parse()

	cfg, err := readConfig(*config)
	checkError(err)

	if *verbose {
		fmt.Println("Metar:", cfg.Metar)
		fmt.Println("MQTT:", cfg.Mqtt)
		fmt.Println("Bind:", cfg.Bind)
	}
	for _, bind := range cfg.Bind {
		metar, err := wemon.GetMetar(cfg.Metar.URL, bind.Icao)
		checkError(err)

		if val, ok := metar[bind.Check]; ok {
			if *verbose {
				fmt.Printf("%s[%s]: %s -> %s\n", bind.Icao, bind.Check, val, bind.Topic)
			}
			mq, err := wemon.NewMqttPublisher(cfg.Mqtt.Host, bind.Topic)
			checkError(err)

			defer mq.Close()

			mq.Pub([]byte(val.(string)))
		}
	}
}
