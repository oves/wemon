package wemon

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// ConfigMetar is a metar configuration
type ConfigMetar struct {
	URL string `yaml:"url"`
}

// Metar is type of returning information
type Metar map[string]interface{}

// GetMetar returns Metar for specific loaction (ICAO format)
func GetMetar(url string, icao string) (Metar, error) {

	var link bytes.Buffer
	link.WriteString(url)
	link.WriteString(icao)

	resp, err := http.Get(link.String())
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	metar := make(Metar)
	err = json.Unmarshal(body, &metar)

	if err != nil {
		return nil, err
	}

	return metar, nil
}
