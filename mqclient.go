package wemon

import (
	"net"

	proto "github.com/huin/mqtt"
	"github.com/jeffallen/mqtt"
)

// ConfigMqtt is a mqtt configuration
type ConfigMqtt struct {
	Host string `yaml:"host"`
}

// Publisher is general interface to publish data
type Publisher interface {
	Pub(data []byte)
	Close()
}

// MqttPublisher is a publisher to mqtt
type MqttPublisher struct {
	client *mqtt.ClientConn
	topic  string
}

// Pub sends data
func (mq MqttPublisher) Pub(data []byte) {
	mq.client.Publish(&proto.Publish{
		Header:    proto.Header{Retain: true},
		TopicName: mq.topic,
		Payload:   proto.BytesPayload(data),
	})
}

// Close finalises the publishing
func (mq MqttPublisher) Close() {
	mq.client.Disconnect()
}

// NewMqttPublisher creates new mqtt publisher
func NewMqttPublisher(host string, topic string) (Publisher, error) {
	conn, err := net.Dial("tcp", host)
	if err != nil {
		return nil, err
	}

	mq := &MqttPublisher{mqtt.NewClientConn(conn), topic}

	if err := mq.client.Connect("", ""); err != nil {
		return nil, err
	}

	// convert MqttPublisher to Publisher
	pub := Publisher(mq)

	return pub, nil
}
